import Service.InfoOperations;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


public class TelegramBot extends TelegramLongPollingBot {

    public TelegramBot(String apiKey, String chatId){
        telegramBotAPIKey = apiKey;
        telegramBotChatId = chatId;
    }

    InfoOperations infoOperations = new InfoOperations();

    private String telegramBotAPIKey = "";
    private String telegramBotChatId = "";

    private final static String PAYMENT_LIST = "Payments list";
    private final static String ADD_PAYMENT = "Add payout";
    private final static String UPDATE_PAYMENT = "Update payout";

    private final static String PAID_SUM = "Total paid";
    private final static String GET_TOTAL_SUM = "Total credit";
    private final static String GET_STAYED_SUM = "Get remains sum";
    private final static String CONFIRM_ADDING = "Confirm add!";
    private final static String CONFIRM_UPDATE = "Confirm update!";
    private final static String GO_TO_MAIN = "Go to main menu...";
    private final static String GET_AVG_PAYOUT = "Get average payout value";
    private String addDataBaseLine = "";
    private String updateDataBaseLine = "";

    @Override
    public void onUpdateReceived(Update update) {
       // long chat_id = update.getMessage().getChatId();
        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            switch (message.getText()) {

                case ADD_PAYMENT:
                    sendMsg_add(message,
                            "Send data to adding and press \""+CONFIRM_ADDING+"\"\n"+
                                    "Format <dd.mm.YYYY> <sumOfPayment>\n" +
                                    "Example: 01.01.2000 1500.11\n"+
                                    "After sending press \""+CONFIRM_ADDING+"\" button.");
                    break;

                case CONFIRM_ADDING:
                    if(addDataBaseLine.equals("")){
                        sendMsg_add(message, "Don't understand your info.\n " +
                                "Please, input in correct format and send: <dd.mm.YYYY> <sumOfPayment>\n" +
                                "Example: 01.01.2000 1500.11\n" +
                                "After sending press \""+CONFIRM_ADDING+"\" button.");
                    }else{
                        infoOperations.addToDataBase(addDataBaseLine);
                        sendMsg_main(message, "Payment was added!");
                        sendMsg_main(message,infoOperations.getListOfPayments());
                    }
                    addDataBaseLine = "";
                    break;

                case UPDATE_PAYMENT:
                    sendMsg_update(message,
                            "Send data to update and press \""+CONFIRM_UPDATE+"\"\n"+
                                    "Format <№> <dd.mm.YYYY> <sumOfPayment>\n" +
                                    "Example: 14 01.01.2000 1500.11\n"+
                                    "After sending press \""+CONFIRM_UPDATE+"\" button.");
                    break;

                case CONFIRM_UPDATE:
                    if(updateDataBaseLine.equals("")){
                        sendMsg_update(message, "Don't understand your info.\n " +
                                "Please, input in correct format and send:  <№> <dd.mm.YYYY> <sumOfPayment>\n" +
                                "Example: 14 01.01.2000 1500.11\n" +
                                "After sending press \""+CONFIRM_UPDATE+"\" button.");
                    }else{
                        infoOperations.updateDataBase(updateDataBaseLine);
                        sendMsg_main(message, "Payment was updated!");
                        sendMsg_main(message,infoOperations.getListOfPayments());
                    }
                    updateDataBaseLine = "";
                    break;

                case GO_TO_MAIN:
                    sendMsg_main(message,"Back to main menu...");
                    break;

                case PAID_SUM:
                    sendMsg_main(message, infoOperations.getPaidSum());
                    break;

                case PAYMENT_LIST:
                    sendMsg_main(message, infoOperations.getListOfPayments());
                    break;

                case GET_TOTAL_SUM:
                    sendMsg_main(message,infoOperations.getSumOfTotal());
                    break;

                case GET_STAYED_SUM:
                    sendMsg_main(message, infoOperations.getSumOfRemainder());
                    break;
                case GET_AVG_PAYOUT:
                    sendMsg_main(message, infoOperations.getAvgPayout());
                    break;

                default:
                    if(message.getText().matches("^([0-3]{1}\\d{1})[.]([0]{1}\\d{1}|[1]{1}[0-2]{1})[.](20{1}\\d{2})\\s{1}(\\d{3,6}[.]\\d{1,2}|\\d{3,6})")){
                        addDataBaseLine = message.getText();
                        System.out.println("Got message:`"+ addDataBaseLine +"`");
                    }
                    if(message.getText().matches("^(\\d{1,})\\s{1}([0-3]{1}\\d{1})[.]([0]{1}\\d{1}|[1]{1}[0-2]{1})[.](20{1}\\d{2})\\s{1}(\\d{3,6}[.]\\d{1,2}|\\d{3,6})")){
                        updateDataBaseLine = message.getText();
                        System.out.println("Got message:`"+ updateDataBaseLine +"`");
                    }
                    break;
            }
        }
    }

    private String generateChatID(Message message){
        if(telegramBotChatId.equals("")){
            return message.getChatId().toString();
        }else{
            return telegramBotChatId;
        }
    }

    @Override
    public String getBotUsername() {
        // TODO
        return null;
    }

    @Override
    public String getBotToken() {
        // TODO
        return telegramBotAPIKey;
    }

    private void sendMsg_main(Message message, String text) {
        SendMessage sendMesg = new SendMessage();
        sendMesg.enableMarkdown(true);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMesg.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(new KeyboardButton(PAYMENT_LIST));
        keyboardFirstRow.add(new KeyboardButton(ADD_PAYMENT));
        keyboardFirstRow.add(new KeyboardButton(UPDATE_PAYMENT));

        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(new KeyboardButton(PAID_SUM));
        keyboardSecondRow.add(new KeyboardButton(GET_TOTAL_SUM));
        keyboardSecondRow.add(new KeyboardButton(GET_STAYED_SUM));


        KeyboardRow keyboardThirdRow = new KeyboardRow();
        keyboardThirdRow.add(new KeyboardButton(GET_AVG_PAYOUT));

        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        keyboard.add(keyboardThirdRow);

        replyKeyboardMarkup.setKeyboard(keyboard);

        sendMesg.setChatId(generateChatID(message));
        sendMesg.setText(text);
        try {
            execute(sendMesg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendMsg_add(Message message, String text) {
        SendMessage sendMsg = new SendMessage();
        sendMsg.enableMarkdown(true);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMsg.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(new KeyboardButton(CONFIRM_ADDING));

        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(new KeyboardButton(GO_TO_MAIN));

        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);

        replyKeyboardMarkup.setKeyboard(keyboard);

        sendMsg.setChatId(generateChatID(message));
        sendMsg.setText(text);
        try {
            execute(sendMsg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendMsg_update(Message message, String text) {
        SendMessage sendMsg = new SendMessage();
        sendMsg.enableMarkdown(true);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMsg.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(new KeyboardButton(CONFIRM_UPDATE));

        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(new KeyboardButton(GO_TO_MAIN));

        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);

        replyKeyboardMarkup.setKeyboard(keyboard);

        sendMsg.setChatId(generateChatID(message));
        sendMsg.setText(text);
        try {
            execute(sendMsg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
