package Service;

public class InfoOperations extends DataBase {

    public String getAvgPayout(){
        Double avg = (getTotalSUM() - Double.parseDouble(getSumOfPayment()))/(getTotalPaymentCount()- getPaymentCount());
        return "The average volume of the payout:\t"+ round(avg,2)+" $\n" +
                "Paid count:\t"+ getPaymentCount() +"\n"+
                "Remains payouts count:\t"+ (getTotalPaymentCount()- getPaymentCount()) +"\n" +
                "Already paid:\t"+getSumOfPayment()+" $\n" +
                "Remains to pay:\t"+(getTotalSUM() - Double.parseDouble(getSumOfPayment()))+" $";
    }

    public String getPaidSum(){
        return "Total paid sum:\n"+getSumOfPayment()+" $";
    }

    public String getListOfPayments(){
        return "List of payments:\n"+getPaymentsList();
    }

    public String getSumOfTotal(){
        return "Full cost:\n"+getTotalSUM()+" $";
    }

    public String getSumOfRemainder(){
        Double remainder = getTotalSUM() - Double.parseDouble(getSumOfPayment());
        return "Remainder of payments:\n"+remainder+" $";
    }

    public void addToDataBase(String paymentLine){
        String date = paymentLine.replaceFirst(".*(([0-3]{1}\\d{1})[.]([0]{1}\\d{1}|[1]{1}[0-2]{1})[.](20{1}\\d{2})).*","$1");
        System.out.println("Date:"+ date );
        double sum = Double.parseDouble(paymentLine.replaceFirst(".*\\s(\\d{3,6}[.]\\d{1,2}|\\d{3,6}).*","$1"));
        System.out.println("Sum:"+sum);

        insertPayment(date, sum);
    }

    public void updateDataBase(String newPaymentLine){
        int id = Integer.parseInt(newPaymentLine.replaceFirst("^(\\d{1,})\\s{1}.*","$1"));
        System.out.println("ID:"+id);
        String date = newPaymentLine.replaceFirst(".*(([0-3]{1}\\d{1})[.]([0]{1}\\d{1}|[1]{1}[0-2]{1})[.](20{1}\\d{2})).*","$1");
        System.out.println("Date:"+ date );
        double sum = Double.parseDouble(newPaymentLine.replaceFirst(".*\\s(\\d{3,6}[.]\\d{1,2}|\\d{3,6}).*","$1"));
        System.out.println("Sum:"+sum);

        updatePayment(id,date,sum);
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
