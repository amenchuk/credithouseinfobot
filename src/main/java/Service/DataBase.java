package Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class DataBase {

    private static String dataBase = "db/homePayments.db";
    private static String url = "jdbc:sqlite:" + dataBase;

    public static String ID = "ID";
    public static String DATE_OF_PAYMENT = "DATE_OF_PAYMENT";
    public static String SUM_OF_PAYMENT = "SUM_OF_PAYMENT";
    public static String TOTAL_SUM = "TOTAL_SUM";
    public static String TOTAL_COUNT_OF_PAY = "TOTAL_COUNT_OF_PAY";

    /**
     * Connect to the .db database
     * @return the Connection object
     */
    private Connection connect() {
        // SQLite connection string
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /** GET SUM OF PAYMENTS
     SELECT SUM(SUM_OF_PAYMENT) FROM PAYMENTS;
     */
    public String getSumOfPayment(){
        String sql = "SELECT SUM("+SUM_OF_PAYMENT+") FROM PAYMENTS;";
        String result = "";

        try(Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql))
        {
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                result = (rs.getString(1));
            }
        }catch (SQLException e){ System.out.println(e.getMessage()); }
        return result;
    }

    public int getPaymentCount(){
        String sql = "SELECT COUNT("+DATE_OF_PAYMENT+") FROM PAYMENTS;";
        int result = 0;

        try(Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql))
        {
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                result = (rs.getInt(1));
            }
        }catch (SQLException e){ System.out.println(e.getMessage()); }
        return result;
    }

    public String getPaymentsList(){
        String sql = "SELECT * FROM PAYMENTS";
        ArrayList<String> paymentsList = new ArrayList<String>();
        String result = "";
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement()
             ){
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
             while (rs.next()) {
                 paymentsList.add(
                         rs.getInt(ID) +  "\t" +
                         rs.getString(DATE_OF_PAYMENT) + "\t" +
                         rs.getDouble(SUM_OF_PAYMENT));
                }
            for(String payment : paymentsList){
                result += payment+"$\n";
            }
        }catch (SQLException e){ System.out.println(e.getMessage()); }
        return result;
    }

    /** INSERT QUERY
     INSERT INTO PAYMENTS (DATE_OF_PAYMENT, SUM_OF_PAYMENT)
     VALUES ('10.02.2019', 602.50);
    */
    public void insertPayment(String date, Double sum){
        String sql = "INSERT INTO PAYMENTS (" + DATE_OF_PAYMENT + ", "+SUM_OF_PAYMENT+") VALUES(?,?)";
        Connection conn = connect();
        try(PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, date);
            pstmt.setDouble(2, sum);
            pstmt.executeUpdate();
        }catch(SQLException e){ System.out.println(e.getMessage()); }
    }

    /** UPDATE LINE
     UPDATE PAYMENTS
     SET DATE_OF_PAYMENT = "21.12.2018", SUM_OF_PAYMENT = 444.44
     WHERE ID = 3;
     */
    public void updatePayment(int id, String date, Double sum){
        String sql = "UPDATE PAYMENTS \n" +
                " SET DATE_OF_PAYMENT = ?, SUM_OF_PAYMENT = ? \n" +
                " WHERE ID = ?;";
        Connection conn = connect();
        try(PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, date);
            pstmt.setDouble(2, sum);
            pstmt.setInt(3, id);
            pstmt.executeUpdate();
        }catch(SQLException e){ System.out.println(e.getMessage()); }
    }

    //SELECT FROM TOTAL TABLE
    /**
    SELECT TOTAL_SUM FROM TOTAL;
    SELECT TOTAL_COUNT_OF_PAY FROM TOTAL;
    */
    public Double getTotalSUM(){
        String sql = "SELECT "+TOTAL_SUM+" FROM TOTAL";
        Double result = null;
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement()){
            ResultSet rs    = stmt.executeQuery(sql);
            // loop through the result set
            while (rs.next()) {
                result = rs.getDouble(TOTAL_SUM);
            }
        }catch (SQLException e){ System.out.println(e.getMessage()); }
        return result;
    }

    public int getTotalPaymentCount(){
        String sql = "SELECT "+TOTAL_COUNT_OF_PAY+" FROM TOTAL";
        int result = 0;
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement()){
            ResultSet rs    = stmt.executeQuery(sql);
            // loop through the result set
            while (rs.next()) {
                result = rs.getInt(TOTAL_COUNT_OF_PAY);
            }
        }catch (SQLException e){ System.out.println(e.getMessage()); }
        return result;
    }

   /* public void updateTotal(Double sum, int count){
        String sql = "UPDATE TOTAL \n" +
                " SET "+TOTAL_SUM+" = ?, "+TOTAL_COUNT_OF_PAY+" = ? ;";
        Connection conn = connect();
        try(PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setDouble(1, sum);
            pstmt.setInt(2, count);
            pstmt.executeUpdate();
        }catch(SQLException e){ System.out.println(e.getMessage()); }
    }*/

    public void insertTotal(Double sum, int count){
        String sql = "INSERT INTO TOTAL (" + TOTAL_SUM + ", "+TOTAL_COUNT_OF_PAY+") VALUES(?,?)";
        Connection conn = connect();
        try(PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setDouble(1, sum);
            pstmt.setInt(2, count);
            pstmt.executeUpdate();
        }catch(SQLException e){ System.out.println(e.getMessage()); }
    }

    public void createNewDatabase() {
        System.out.println("Try to create a database file.");
        if(new File(dataBase).exists()){
            System.out.println("Database by path `"+dataBase+"` is exists.\nDatabase NOT CREATED!!!");
        }else{
            File file = new File(dataBase);
            file.getParentFile().mkdirs();
                try {
                    FileWriter writer = new FileWriter(file);
                } catch (IOException e) { e.printStackTrace(); }
            try (Connection conn = DriverManager.getConnection(url)) {
                if (conn != null) {
                    DatabaseMetaData meta = conn.getMetaData();
                    System.out.println("The driver name is " + meta.getDriverName());
                    System.out.println("A new database \""+file.getName()+"\" has been created. \n");
                    createTables();
                }
            } catch(SQLException e){ System.out.println(e.getMessage()); }
        }
    }

    private void createTables(){
        String sqlCreateTablePayments = "CREATE TABLE IF NOT EXISTS PAYMENTS (\n"
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, \n"
                + DATE_OF_PAYMENT+" text, \n"
                + SUM_OF_PAYMENT+" real \n);";

        String sqlCreateTableTotal = "CREATE TABLE IF NOT EXISTS TOTAL (\n"
                + TOTAL_SUM+" real, \n"
                + TOTAL_COUNT_OF_PAY+" integer \n);";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()){
            stmt.execute(sqlCreateTablePayments);
            stmt.execute(sqlCreateTableTotal);
        }catch (SQLException e){ System.out.println(e.getMessage()); }

    }





}
