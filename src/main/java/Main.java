import Service.DataBase;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class Main{
    public static void main(String[] args) {
        DataBase dataBase = new DataBase();

        if(args.length == 0){
            System.out.println("FORMAT: java -jar <file_nme>.jar <command>");
            System.out.println("For more info input '--help' as command.");
            System.exit(0);
        }else{
            switch(args[0]){
                case "--runBot" :
                            switch (args.length){
                                case 4:
                                    if(args[2].equals("yes")){
                                        startBot(args[1],args[3]);
                                    }else{System.exit(1);}
                                    break;
                                case 3:
                                    if(args[2].equals("no") ){
                                        startBot(args[1],"");
                                    }else{System.exit(1);}
                                    break;
                                default:
                                    System.out.println("--runBot FORMAT:\n"+
                                            "--runBot\t- run the bot, use with parameters <botAPItoken> <private> <chat_id>\n" +
                                            "\t <botAPItoken> - API token of bot.\n"+
                                            "\t <private> - bot private or not. Values: yes - private, no - for any one\n" +
                                            "\t <chat_id> - id of private chat, only if private.\n");
                                    System.exit(0);
                                    break;
                            }
                    break;
                case "--createDataBase":
                    dataBase.createNewDatabase();
                    System.exit(0);
                    break;

                case "--insertTotals":
                switch (args.length){
                    case 3:
                            dataBase.insertTotal(Double.parseDouble(args[1]),Integer.parseInt(args[2]));
                        System.out.println("Totals were inserted.");
                            break;
                    default:
                        System.out.println("--insertTotals FORMAT:\n"+
                            "--insertTotals\t- insert data to the TOTAL table, use with parameters <total_sum> <total_count>\n" +
                            "\t <total_sum> - total sum of credit, double exmpl.: 35665.22\n"+
                            "\t <total_count> - total count of payments, integer exmpl.: 35\n");
                            System.exit(0);
                            break;
                }
                System.exit(0);
                break;
                case "--help":
                    System.out.println("\nCOMMANDS LIST:\n" +
                            "--runBot\t- run the bot, use with parameters <botAPItoken> <private> <chat_id>\n" +
                            "\t <botAPItoken> - API token of bot.\n" +
                            "\t <private> - bot private or not. Values: yes - private, no - for any one\n" +
                            "\t <chat_id> - id of private chat, only if private.\n" +
                            "--createDataBase\t- for creating database.\n" +
                            "--insertTotals\t- for initialization database table TOTALS, use with parameters <sum> <count_of_payments>\n" +
                            "\t <sum> - double value, total currency amount.\n" +
                            "\t <count_of_payments> - integer value, count of payments to the end.");
                    System.exit(0);
                    break;
                 default:
                     System.out.println("FORMAT: java -jar <file_nme>.jar <command>");
                     System.out.println("For more info input '--help' as command.");
                     System.exit(0);
                     break;
            }
        }
    }

    public static void startBot(String tokenAPI, String chatId){
        System.out.println("Program executing...\nTelegram bot started ");
        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            botsApi.registerBot(new TelegramBot(tokenAPI,chatId));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
